'''
@author: Mohammad Faham Khan
'''
import os.path
import pickle
from pprint import pprint
class Animal:
    '''
    classdocs
    '''
    ##The Get functions have print functions that you can uncomment in case you want to show students that the function is being called

    #Here is the initialization for the Animal Class, you can add/remove perameters, but if you do make sure to
    #do the same for the all the methods that call this class.
    #animalType ex: Mammal
    #foodType ex: Herbavore
    #species ex: Racoon
    def __init__(self, animalType, foodType, species):
        '''
        Constructor
        '''
        self.animalType = animalType
        self.foodType = foodType
        self.species = species

    #This call gets the animal type, such as Mammal
    def getAnimalType(self):
        #print("Getting Animal Type: "+self.animalType)
        return self.animalType

    #This gets the animal diet such as omnivore
    def getFoodType(self):
        #print("Getting Animal Diet: "+self.foodType)
        return self.foodType

    #This gets the species of the animal such as cat
    def getSpecies(self):
        #print("Getting Animal Species: "+self.species)
        return self.species

##Here we create a list in case the serialized pickle file does not exist. It would be a good idea to point out that no pickle file exists prior to you actually saving
items = list()

##Here we check if a file does exist and if it does, we load it into the list.
##Might be a good idea to restart the app after saving to show that object persistance was implemented
if os.path.exists("animals.pickle"):
    pickle_in = open("animals.pickle", "rb")
    items = pickle.load(pickle_in)
print("There are currently "+str(len(items))+" animals")

##This is the basic app where we keep asking the user to create a new animal or display the all the animals
while 1:
    action = input("Show all animals (1) or create a new animal (2) or exit(3)?: ")
    ##Here we desplay all the attributes of the Animal
    if action == "1":
        print("There are currently "+str(len(items))+" animals")
        for i in items:
            print("Type: "+i.getAnimalType()+", Diet: "+i.getFoodType() +", Species: "+i.getSpecies())

    ##Here we simple ask the questions and then create the animal object
    elif action == "2":
        animalType = input("What kind of animal is this (Mammal, Reptile etc.): ")
        foodType = input("What is its diet (Herbivore, Omnivore): ")
        species = input("Species name (Cat, Lion, Monkey): ")
        #Once the questions have been asked, create the Animal Object
        animalCreated = Animal(animalType, foodType, species)
        #Then add the object to the list
        items.append(animalCreated)
        #Now save the list, this is where the serialization magic takes place. You could even move this to the action == "3" to make it more efficient.
        pickle_out = open("animals.pickle", "wb")
        pickle.dump(items, pickle_out)
        pickle_out.close()
        print("Animal added to list and the data is saved!")

    #Basic ending the program
    elif action == "3":
        break
