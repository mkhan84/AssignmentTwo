Name: Mohammad Faham Khan
ID:1082794
Email: mkhan84@uoguelph.ca
###########Instructions  
Install python 3.5 or above and execute the application. 
To do so please direct yourself to the folder that contains the application and execute the following command: 
python main.py

To install python use: 
https://www.python.org/downloads/

###########How I have used the best practices in OOP 
I made use of inheritance. The concept of this app is to have a clothing store inventory that can hold 3 types of items. 
All 3 items have similar attributes, and for that I created a clothingitem class. From there each item inherits that class and 
adds its own attributes, such as shirt has "sizeUS" which ranges from S,M,L while pants has waist and length etc. 
To see these you can take a look at the following files: 
clothingitem.py
pants.py
shirt.py
sweater.py

###########How I have portability
By selecting an item from the inventory list, you can export the item and copy it to your clipboard. 
This takes place in the file called: GUI.py
The line numbers are: 203-214
The lines basically show a window that displays the object in a json format, and allows the user to copy. 


###########How I have persistence 
By clicking save, the entire list is serialized and whenever the app is reopened the serialized file is opened and the saved list is recreated. You don't need to load, the data, it automatically loads your last save whenever you start up the application again. 

This takes place in the file called: GUI.py
The line numbers are: 28-32, 197-201
28-32:The first set of lines basically check to see if a serialized file (pickle file) exists, if not then it creates a default array to display, but if the file exists it deserializes and inserts the objects into the list
197-201: These set of lines serialize the objects within the list and save them to items.pickle