'''
Created on Feb 10, 2019

@author: 
'''
from tkinter import *
import os.path
import pickle
from pprint import pprint

from shirt import Shirt
from pants import Pants
from sweater import Sweater


class GUI(object):
    '''
    classdocs
    '''

    def __init__(self, root):
        '''
        Constructor
        '''
        self.incrementNumber = 0;
        self.root = root

        self.items = [Shirt("Black", "Rugged", "M", "False"), Pants("Grey", "Jeans", "34", "47"),
                      Pants("White", "Jeans", "33", "42"), Sweater("Red", "Cotton", "L", "False")]
        if os.path.exists("items.pickle"):
            pickle_in = open("items.pickle", "rb")
            self.items = pickle.load(pickle_in)

        root.title("Clothing Store Inventory")
        root.geometry("600x200")
        GUI.createWidgets(self)
        root.mainloop()

    def createWidgets(self):
        self.topFrame = Frame(self.root)
        self.topFrame.pack()

        self.bottomFrame = Frame(self.root)
        self.bottomFrame.pack(fill=X)

        self.buttonPants = Button(self.topFrame, text="Add Pants", command=self.createPantsAdd)
        self.buttonShirt = Button(self.topFrame, text="Add Shirt", command=self.createShirtAdd)
        self.buttonSweater = Button(self.topFrame, text="Add Sweater", command=self.createSweaterAdd)
        self.buttonRemove = Button(self.topFrame, text="Remove", command=self.deleteItem)
        self.buttonSave = Button(self.topFrame, text="Save", command=self.save)

        self.buttonPants.grid(row=0, column=0)
        self.buttonShirt.grid(row=0, column=1)
        self.buttonSweater.grid(row=0, column=2)
        self.buttonRemove.grid(row=0, column=3)
        self.buttonSave.grid(row=0, column=4)

        self.buttonExport = Button(self.bottomFrame, text="Export", command=self.export)
        self.buttonExport.pack(fill=X)

        self.itemsList = Listbox(self.bottomFrame)
        itemDisplay = ""
        for i in self.items:
            for key in sorted(i.returnJSON()):
                itemDisplay = itemDisplay + key + ": " + str(i.returnJSON()[key]) + ", "
            self.itemsList.insert(END, itemDisplay)
            itemDisplay = ""

        self.itemsList.pack(fill=X)

    def deleteItem(self):
        selection = self.itemsList.curselection()
        self.itemsList.delete(selection[0])
        del self.items[selection[0]]
        print(self.items)

    def createShirtAdd(self):
        top = Toplevel()
        self.colourLabel = Label(top, text="Colour")
        self.textureLabel = Label(top, text="Texture")
        self.sizeLabel = Label(top, text="SizeUS (S,M,L)")
        self.tshirtLabel = Label(top, text="T-Shirt?")

        self.colourLabel.grid(row=0, column=0)
        self.colourEntry = Entry(top)
        self.colourEntry.grid(row=0, column=1)

        self.textureLabel.grid(row=1, column=0)
        self.textureEntry = Entry(top)
        self.textureEntry.grid(row=1, column=1)

        self.sizeLabel.grid(row=2, column=0)
        self.sizeEntry = Entry(top)
        self.sizeEntry.grid(row=2, column=1)

        self.tshirtLabel.grid(row=3, column=0)
        self.tshirtEntry = Entry(top)
        self.tshirtEntry.grid(row=3, column=1)

        buttonAdd = Button(top, text="Add", command=self.shirtAdd)
        buttonAdd.grid(row=4, column=0)
        top.mainloop()

    def shirtAdd(self):
        tshirt = self.tshirtEntry.get()
        sizeUS = self.sizeEntry.get()
        texture = self.textureEntry.get()
        colour = self.colourEntry.get()
        item = Shirt(colour, texture, sizeUS, tshirt)
        self.items.append(item)
        itemDisplay = ""
        for key in sorted(item.returnJSON()):
            itemDisplay = itemDisplay + key + ": " + str(item.returnJSON()[key]) + ", "
        self.itemsList.insert(END, itemDisplay)

    def createSweaterAdd(self):
        top = Toplevel()

        colourLabel = Label(top, text="Colour")
        textureLabel = Label(top, text="Texture")
        sizeLabel = Label(top, text="SizeUS (S,M,L)")
        hoodedLabel = Label(top, text="Hooded?")

        colourLabel.grid(row=0, column=0)
        self.colourEntry = Entry(top)
        self.colourEntry.grid(row=0, column=1)

        textureLabel.grid(row=1, column=0)
        self.textureEntry = Entry(top)
        self.textureEntry.grid(row=1, column=1)

        sizeLabel.grid(row=2, column=0)
        self.sizeEntry = Entry(top)
        self.sizeEntry.grid(row=2, column=1)

        hoodedLabel.grid(row=3, column=0)
        self.hoodedEntry = Entry(top)
        self.hoodedEntry.grid(row=3, column=1)

        buttonAdd = Button(top, text="Add", command=self.sweaterAdd)
        buttonAdd.grid(row=4, column=0)

        top.mainloop()

    def sweaterAdd(self):
        hooded = self.hoodedEntry.get()
        sizeUS = self.sizeEntry.get()
        texture = self.textureEntry.get()
        colour = self.colourEntry.get()
        item = Sweater(colour, texture, sizeUS, hooded)
        self.items.append(item)
        itemDisplay = ""
        for key in sorted(item.returnJSON()):
            itemDisplay = itemDisplay + key + ": " + str(item.returnJSON()[key]) + ", "
        self.itemsList.insert(END, itemDisplay)

    def createPantsAdd(self):
        top = Toplevel()
        colourLabel = Label(top, text="Colour")
        textureLabel = Label(top, text="Texture")
        waistLabel = Label(top, text="Waist (IN)")
        lengthLabel = Label(top, text="Length (IN)")

        colourLabel.grid(row=0, column=0)
        self.colourEntry = Entry(top)
        self.colourEntry.grid(row=0, column=1)

        textureLabel.grid(row=1, column=0)
        self.textureEntry = Entry(top)
        self.textureEntry.grid(row=1, column=1)

        waistLabel.grid(row=2, column=0)
        self.waistEntry = Entry(top)
        self.waistEntry.grid(row=2, column=1)

        lengthLabel.grid(row=3, column=0)
        self.lengthEntry = Entry(top)
        self.lengthEntry.grid(row=3, column=1)

        buttonAdd = Button(top, text="Add", command=self.pantsAdd)
        buttonAdd.grid(row=4, column=0)

        top.mainloop()

    def pantsAdd(self):
        waist = self.waistEntry.get()
        length = self.lengthEntry.get()
        texture = self.textureEntry.get()
        colour = self.colourEntry.get()
        item = Sweater(colour, texture, waist, length)
        self.items.append(item)
        itemDisplay = ""
        for key in sorted(item.returnJSON()):
            itemDisplay = itemDisplay + key + ": " + str(item.returnJSON()[key]) + ", "
        self.itemsList.insert(END, itemDisplay)

    def save(self):
        pickle_out = open("items.pickle", "wb")
        pickle.dump(self.items, pickle_out)
        pickle_out.close()
        print("Saved")

    def export(self):
        self.topExport = Toplevel()
        selection = self.itemsList.curselection()
        self.exportVal = str(self.items[selection[0]].returnJSON()).replace("'", "\"")

        exportLabel = Label(self.topExport, text=self.exportVal)
        exportLabel.pack()

        buttonAdd = Button(self.topExport, text="Copy To Clipboard", command=self.copyToClipBoard)
        buttonAdd.pack()

        self.topExport.mainloop()

    def copyToClipBoard(self):
        clip = Tk()
        clip.withdraw()
        clip.clipboard_clear()
        clip.clipboard_append(self.exportVal)
        clip.destroy()
