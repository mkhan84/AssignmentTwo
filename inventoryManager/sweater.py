'''
Created on Feb 9, 2019

@author: 
'''
from clothingItem import clothingItem

class Sweater(clothingItem):
    '''
    classdocs
    '''

    def __init__(self, colour, texture, sizeUS, hooded):
        '''
        Constructor
        '''
        clothingItem.__init__(self, "Sweater", colour, texture)
        self.sizeUS = sizeUS
        self.hooded = hooded
        
    def getSize(self):
        return self.sizeUS