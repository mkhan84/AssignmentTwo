'''
Created on Feb 9, 2019

@author: 
'''
from clothingItem import clothingItem

class Shirt(clothingItem):
    '''
    classdocs
    '''


    def __init__(self, colour, texture, sizeUS, tshirt):
        '''
        Constructor
        '''
        clothingItem.__init__(self, "Shirt", colour, texture)
        self.sizeUS = sizeUS
        self.tshirt = tshirt ##This is a boolean value
        
    def getSize(self):
        return self.sizeUS
        
        