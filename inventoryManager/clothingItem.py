'''
Created on Feb 9, 2019

@author: Mohammad Faham Khan
This is a base class for all the clothing items in the store.
This class contains, colour and texture.
'''

from pprint import pprint


class clothingItem:
    '''
    classdocs
    '''


    def __init__(self, itemType, colour, texture):
        '''
        Constructor
        '''
        self.itemType = itemType
        self.colour = colour
        self.texture = texture

    def getType(self):
        print("Getting Type: "+self.itemType)
        return self.itemType

    def getColour(self):
        print("Getting Colour: "+self.colour)
        return self.colour

    def getTexture(self):
        print("Getting Texture: "+self.colour)
        return self.colour

    def returnJSON(self):
        return vars(self)