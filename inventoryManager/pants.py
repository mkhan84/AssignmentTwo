'''
Created on Feb 9, 2019

@author: 
'''
from clothingItem import clothingItem
class Pants(clothingItem):
    '''
    classdocs
    '''


    def __init__(self, colour, texture, waist, length):
        '''
        Constructor
        '''
        clothingItem.__init__(self, "Pants", colour, texture)
        self.waist=waist
        self.length = length

    def getLength(self):
        return self.length

    def getWaist(self):
        return self.waist
    
    